<?php
global $themebucket_firebrick;
?>
<?php if ($themebucket_firebrick['section_contact_display']) { ?>
    <!--section contact start-->
    <section id="contact" class="contact">
        <div>
            <h2 class="section-header contact-header wow bounceIn"><?php if (isset($themebucket_firebrick['section_contact_title'])) echo $themebucket_firebrick['section_contact_title']; ?></h2>

            <p class="section-intro wow fadeInUp">
                <?php if (isset($themebucket_firebrick['section_contact_subtitle'])) echo $themebucket_firebrick['section_contact_subtitle']; ?>
            </p>
        </div>
        <div id="location-map">
            <div id="map">
            </div>
            <div id="marker-content">
                <?php if (isset($themebucket_firebrick['section_contact_description'])) echo wpautop($themebucket_firebrick['section_contact_description']); ?>
            </div>
        </div>
    </section>
    <!--section contact end-->
<?php } ?>